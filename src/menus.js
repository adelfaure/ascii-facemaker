/*
    ASCII Facemaker, copyright (C) 2021 Adel Faure, contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// TO PASS SOMEWHERE ELSE ?

function selectText(node) {

    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.warn("Could not select text in node: Unsupported browser.");
    }
}

// MENUS

var commands = [];

// Intro

function intro_menu() {
  commands = [];
  log.textContent = '';
  
  add_to_print('Version 1.0 2021-10-12\n');
  print_asset('intro');
  randomize();
  print_face();
  add_to_print('\n');

  // MENU
  add_to_print('[ '+commands.length+' ] Start (Type the command number between brackets then press ENTER)\n');
  commands.push(function(){ 
    clear();
    main_menu(); 
  });
}

// Main

function main_menu() {
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ options_menu(); });
  
  add_to_print('Main menu\n');
  print_face();
  add_to_print('\n');

  // MENU
  add_to_print('[ '+commands.length+' ] Face\n');
  commands.push(function(){ change_face_menu(); });
  add_to_print('[ '+commands.length+' ] Pilosity\n');
  commands.push(function(){ change_pilosity_menu(); });
  add_to_print('[ '+commands.length+' ] Accessories\n');
  commands.push(function(){ change_accessories_menu(); });
  add_to_print('[ '+commands.length+' ] Randomize\n');
  commands.push(function(){ 
    randomize();
    main_menu();
  });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Options\n');
}

function options_menu(){
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ main_menu(); });
  
  add_to_print('Options menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Save face\n');
  commands.push(function(){ save_face_menu(); });
  add_to_print('[ '+commands.length+' ] Change colors\n');
  commands.push(function(){ change_colors_menu(); });
  add_to_print('[ '+commands.length+' ] Change font\n');
  commands.push(function(){ change_font_menu(); });
  add_to_print('[ '+commands.length+' ] About ASCII Face Maker\n');
  commands.push(function(){ about_menu(); });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to main menu\n');
}

function about_menu(){
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ options_menu(); });
  
  add_to_print('ASCII Face Maker, copyright (C) 2021 Adel Faure, contact@adelfaure.net, https://gitlab.com/adelfaure/ascii-facemaker\n\nThis program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.\n');
  add_to_print('\n');
  add_to_print('I\'d like to thank Cthulu for motivating me with this idea of a "Facemaker for the 21st century". Mistfunk, Textmode Friends, SoloDevlopment members and people who follow me on twitter for their warm support and encouragement.\n');
  add_to_print('\n');
  add_to_print('Licenses: jgs5 and jgs9 are under SIL Open Font License 1.1, TopazPlus is under GPL-FE and UbuntuMono is under Ubuntu Font Licence 1.0. The javascript library html2canvas 1.3.2 is used fo the png export copyright (c) 2021 Niklas von Hertzen under MIT License.\n');
  
  add_to_print('[ 0 ] Back to options menu\n');
}

function change_font_menu(){
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ options_menu(); });
  
  add_to_print('Change font menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Use jgs5 '+(font=='jgs5'?'(current)':'')+'\n');
  commands.push(function(){
    font = 'jgs5';
    change_font_menu(); 
  });
  add_to_print('[ '+commands.length+' ] Use jgs9 '+(font=='jgs9'?'(current)':'')+'\n');
  commands.push(function(){
    font = 'jgs9';
    change_font_menu(); 
  });
  add_to_print('[ '+commands.length+' ] Use TopazPlus '+(font=='topaz'?'(current)':'')+'\n');
  commands.push(function(){
    font = 'topaz';
    change_font_menu(); 
  });
  add_to_print('[ '+commands.length+' ] Use UbuntuMono '+(font=='ubuntu'?'(current)':'')+'\n');
  commands.push(function(){
    font = 'ubuntu';
    change_font_menu(); 
  });
  commands.push(function(){ change_font_menu(); });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to options menu\n');
}

var scale = 2;

function save_face_menu(){
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ options_menu(); });
  
  add_to_print('Change colors menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Download as PNG file\n');
  commands.push(function(){
    log.style.backgroundColor = document.body.style.backgroundColor;
    let lines = log.textContent.split('\n');
    lines.shift();
    lines.shift();
    log.textContent = lines.join('\n');
    log.style.fontSize = font_width * 2 + 'px';
    log.style.lineHeight = font_width * 2 + 'px';
    log.style.paddingLeft = Math.floor(1.5 * font_width) + 'px';
    log.style.paddingRight = Math.floor(1.5 * font_width) + 'px';
    log.style.width = 21 * font_width + 'px';
    log.style.height = 24 * font_width + 'px';
    log.style.transform = 'scale(' + scale + ')';
    html2canvas(log).then(function(canvas) {
      let data_uri = canvas.toDataURL('png');
      let data_link = document.createElement('a');
      data_link.href = data_uri;
      data_link.setAttribute('download', 'ascii_face');
      data_link.click();
    });
    log.style.transform = 'none';
    log.style.backgroundColor = 'transparent';
    save_face_menu();
  });
  add_to_print('[ '+commands.length+' ] Change PNG scale (currently '+(scale)+', max = 4)\n');
  commands.push(function(){
    if (scale < 4){
      scale++;
    } else {
      scale = 1;
    }
    save_face_menu(); 
  });
  add_to_print('[ '+commands.length+' ] Copy as text\n');
  commands.push(function(){ 
    let lines = log.textContent.split('\n');
    lines.shift();
    lines.shift();
    lines.pop();
    lines.pop();
    lines.pop();
    lines.pop();
    lines.pop();
    lines.pop();
    log.textContent = lines.join('\n');
    selectText(log);
    document.execCommand("copy");
    save_face_menu(); 
  });

  add_to_print('\n');
  add_to_print('[ 0 ] Back to options menu\n');
}

function change_colors_menu() {
  commands = [];
  log.textContent = '';
  // options
  commands.push(function(){ options_menu(); });
  
  add_to_print('Change colors menu\n');
  print_face();
  add_to_print('\n');
  
  if (mode == 'day') {
    add_to_print('[ '+commands.length+' ] Switch to night mode\n');
    commands.push(function(){
      mode = 'night';
      change_colors_menu(); 
    });
  } else {
    add_to_print('[ '+commands.length+' ] Switch to day mode\n');
    commands.push(function(){
      mode = 'day';
      change_colors_menu(); 
    });
  }
  add_to_print('[ '+commands.length+' ] Change font color\n');
  commands.push(function(){
    change_color_menu('font color'); 
  });
  add_to_print('[ '+commands.length+' ] Change background color\n');
  commands.push(function(){
    change_color_menu('background color'); 
  });
  add_to_print('[ '+commands.length+' ] Randomize colors\n');
  commands.push(function(){ 
    randomize_colors();
    change_colors_menu();
  });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to options menu\n');
}

function change_color_menu(what){
  commands = [];
  log.textContent = '';
  // back
  commands.push(function(){ change_colors_menu(); });
  
  let to_change = mode == 'night'? what == 'font color'? 'bright': 'dark'
    : what == 'font color'? 'dark': 'bright';
  let value = to_change == 'dark'? dark_color_index: bright_color_index; 

  add_to_print('Change '+what+' menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('Enter a number between 1 and 8 (currently '+(value+1)+')\n');

  for (var i = 0; i < 8; i++) {
    let number = i;
    commands.push(function(){
      if (to_change == 'dark'){
        dark_color_index = number;
      } else {
        bright_color_index = number;
      }
      change_color_menu(what); 
    });
  }
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to change colors menu\n');
}

function change_accessories_menu() {
  commands = [];
  log.textContent = '';
  // back
  commands.push(function(){ main_menu(); });
  
  add_to_print('Accessories menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Clothes\n');
  commands.push(function(){ change_menu('cloth', change_accessories_menu, 'accessories'); });
  add_to_print('[ '+commands.length+' ] Glasses\n');
  commands.push(function(){ change_menu('glasses', change_accessories_menu, 'accessories'); });
  add_to_print('[ '+commands.length+' ] Hat\n');
  commands.push(function(){ change_menu('hat', change_accessories_menu, 'accessories'); });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to main menu\n');
}

function change_pilosity_menu(){
  commands = [];
  log.textContent = '';
  // back
  commands.push(function(){ main_menu(); });
  
  add_to_print('Pilosity menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Eyebrows\n');
  commands.push(function(){ change_menu('eyebrows', change_pilosity_menu, 'pilosity'); });
  add_to_print('[ '+commands.length+' ] Beard\n');
  commands.push(function(){ change_menu('beard', change_pilosity_menu, 'pilosity'); });
  add_to_print('[ '+commands.length+' ] Hair\n');
  commands.push(function(){ change_menu('hair', change_pilosity_menu, 'pilosity'); });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to main menu\n');
}

function change_face_menu(){
  commands = [];
  log.textContent = '';
  // back
  commands.push(function(){ main_menu(); });
  
  add_to_print('Face menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('[ '+commands.length+' ] Chin\n');
  commands.push(function(){ change_menu('chin', change_face_menu, 'face'); });
  add_to_print('[ '+commands.length+' ] Noze\n');
  commands.push(function(){ change_menu('noze', change_face_menu, 'face'); });
  add_to_print('[ '+commands.length+' ] Eyes\n');
  commands.push(function(){ change_menu('eyes', change_face_menu, 'face'); });
  add_to_print('[ '+commands.length+' ] Mouth\n');
  commands.push(function(){ change_menu('mouth', change_face_menu, 'face'); });
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to main menu\n');
}

function change_menu(what, to, from){
  commands = [];
  log.textContent = '';
  // back
  commands.push(function(){ to(); });
  
  add_to_print('Change '+what+' menu\n');
  print_face();
  add_to_print('\n');
  
  add_to_print('Enter a number between 1 and '+(face_limits[what]+1)+' (currently '+(face[what]+1)+')\n');
  for (var i = 0; i < face_limits[what]+1; i++) {
    let number = i;
    commands.push(function(){
      face[what] = number;
      change_menu(what, to, from); 
    });
  }
  
  add_to_print('\n');
  add_to_print('[ 0 ] Back to '+from+' menu\n');
}
